# Geospatial Web

<http://www.geospatialweb.ca>

Development sandbox website to showcase the code integration of NestJS, Angular 9, RxJS, Mapbox GL, Deck.GL, PostGIS 3.0 and Docker. This implementation features dedicated Map instances in separate routes and `Angular Model` simple state management with immutable data exposed as a RxJS Observable: <https://tomastrajan.github.io/angular-model-pattern-example#/about>.
