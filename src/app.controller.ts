import { Controller, Get, Redirect } from '@nestjs/common';

@Controller()
export class AppController {
  constructor() {}

  @Get('*map')
  @Redirect('/', 301)
  onRefreshRedirect(): string {
    return '/';
  }
}
