import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ServeStaticModule } from '@nestjs/serve-static';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ApiModule } from './api/api.module';

import { AppController } from './app.controller';

@Module({
  controllers: [AppController],
  imports: [
    ApiModule,
    ConfigModule.forRoot({
      isGlobal: true
    }),
    ServeStaticModule.forRoot(),
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'db',
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
      synchronize: false /* production setting */
    })
  ],
  providers: []
})
export class AppModule {}
