import { Controller, Get, Header, Query, UseGuards, UseInterceptors, ValidationPipe } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { ApiService } from '../services/api.service';
import { GeojsonInterceptor } from '../interceptors/geojson.interceptor';
import { QueryParamsDto } from '../dtos/query-params.dto';

@UseGuards(AuthGuard('jwt'))
@Controller('api')
export class ApiController {
  constructor(private readonly apiService: ApiService) {}

  @Get('geojson')
  @UseInterceptors(GeojsonInterceptor)
  async getGeojson(@Query(ValidationPipe) params: QueryParamsDto): Promise<Array<{ geojson: string }>> {
    return await this.apiService.getGeojson(params);
  }

  @Get('mapbox-access-token')
  @Header('Content-Type', 'application/json')
  getMapboxAccessToken(): string {
    return this.apiService.getMapboxAccessToken();
  }
}
