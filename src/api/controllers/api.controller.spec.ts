import { Test, TestingModule } from '@nestjs/testing';

import { ApiController } from './api.controller';
import { ApiService } from '../services/api.service';
import { GeojsonService } from '../services/geojson/geojson.service';

describe('ApiController', () => {
  let apiController: ApiController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [ApiController],
      providers: [ApiService, GeojsonService]
    }).compile();

    apiController = app.get<ApiController>(ApiController);
  });

  describe('accessToken', () => {
    it(`should return ${process.env.ACCESSTOKEN}`, () => {
      expect(apiController.getMapboxAccessToken()).toBe(process.env.ACCESSTOKEN);
    });
  });
});
