import { IsString } from 'class-validator';

export class QueryParamsDto {
  @IsString()
  readonly fields: string;

  @IsString()
  readonly table: string;
}
