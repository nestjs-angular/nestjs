import { AuthMiddleware } from './auth.middleware';

describe('IdentityMiddleware', () => {
  it('should be defined', () => {
    expect(new AuthMiddleware(null)).toBeDefined();
  });
});
