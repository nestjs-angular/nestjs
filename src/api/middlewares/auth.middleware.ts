import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';
import { NextFunction } from 'express-serve-static-core';

import { LocalStrategy } from '../../auth/strategies/local.strategy';

@Injectable()
export class AuthMiddleware implements NestMiddleware {
  constructor(private readonly localStrategy: LocalStrategy) {}

  use(req: Request, _: Response, next: NextFunction) {
    (req as any).isAuth = this.localStrategy.isAuthenticated;
    console.log(`${req.method}  ${req.url}: ${(req as any).isAuth.toString().toUpperCase()}`);
    next();
  }
}
