import { Injectable } from '@nestjs/common';
import { Pool, QueryResult } from 'pg';

import { QueryParamsDto } from '../dtos/query-params.dto';

@Injectable()
export class ApiService {
  constructor() {}

  getGeojson(params: QueryParamsDto): Promise<Array<{ geojson: string }>> {
    const query: string = `
      SELECT ST_AsGeoJSON(feature.*) AS geojson
      FROM (
        SELECT ${params.fields}
        FROM ${params.table}
      ) AS feature
    `;

    const pool: Pool = new Pool({
      /* docker instance: process.env.DATABASE_URI */
      /* local instance:  process.env.DATABASE_URI_LOCAL */
      connectionString: process.env.DATABASE_URI
    });

    return new Promise((resolve: any): void => {
      pool
        .query(query)
        .then((result: QueryResult): void => {
          resolve(result.rows);
          pool.end();
        })
        .catch((err: Error): void => {
          console.error('Query Failed:\n', err);
          pool.end();
        });
    });
  }

  getMapboxAccessToken(): string {
    return JSON.stringify(process.env.MAPBOX_ACCESS_TOKEN);
  }
}
