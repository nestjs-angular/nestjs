import { Injectable } from '@nestjs/common';
import { FeatureCollection, Feature } from 'geojson';

@Injectable()
export class GeojsonService {
  constructor() {}

  createFeatureCollection(features: Array<{ geojson: string }>): FeatureCollection {
    const fc: FeatureCollection = {
      type: 'FeatureCollection',
      features: []
    };

    if (features.length) {
      fc.features = features.map((feature: { geojson: string }): Feature => JSON.parse(feature.geojson));
    }

    return fc;
  }
}
